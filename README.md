# README #

# Account/Repository Migration Notice! #

## Where is the sdlab repo now? ##

* The tudor_p bitbucket.org account was renamed to tudor-p because of technical reasons.
* The new link for the sdlab repo is: [https://bitbucket.org/tudor-p/sdlab](https://bitbucket.org/tudor-p/sdlab)
* The current account tudor_p is a new account and now represents just a placeholder to redirect the traffic to the new name of the original account
* Updated on 2018-01-10

### Why the account was renamed? ###

* Some time ago bitbucket.org changed their website hosting policy...
* Since then accounts with name containing _ (down scroll), like the tudor_p name, cannot be used for hosting static websites by bitbucket.
* [Details on current policy for hosting static websites with Bitbucket](https://confluence.atlassian.com/bitbucket/publishing-a-website-on-bitbucket-cloud-221449776.html)